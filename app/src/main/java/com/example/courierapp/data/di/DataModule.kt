package com.example.courierapp.data.di

import android.content.Context
import com.example.courierapp.data.LoginAPIHelper
import com.example.courierapp.data.LoginAuthenticator
import com.example.courierapp.data.LoginRepositoryImplementation
import com.example.courierapp.data.storage.SharedPrefsUserStorage
import com.example.courierapp.data.storage.UserStorage
import com.example.courierapp.domain.repository.LoginRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataModule {

    @Provides
    fun providesBaseUrl(): String = "https://dev-is.tick-time.ru/"

    @Provides
    fun provideLoginAuthenticator(loginRepository: LoginRepository): LoginAuthenticator {
        return LoginAuthenticator(loginRepository)
    }

    @Provides
    fun provideClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor()
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        return OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .build()
    }

    @Provides
    @Singleton
    fun provideLoginRetrofit(BASE_URL: String, client: OkHttpClient): Retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(client)
        .addConverterFactory(MoshiConverterFactory.create())
        .build()

    @Provides
    @Singleton
    fun provideLoginAPIHelper(loginRetrofit: Retrofit): LoginAPIHelper {
        return LoginAPIHelper(loginRetrofit = loginRetrofit)
    }

    @Provides
    @Singleton
    fun provideUserStorage(@ApplicationContext context: Context): UserStorage {
        return SharedPrefsUserStorage(context)
    }

    @Provides
    @Singleton
    fun provideLoginRepository(
        userStorage: UserStorage,
        loginAPIHelper: LoginAPIHelper
    ): LoginRepository {
        return LoginRepositoryImplementation(userStorage, loginAPIHelper)
    }
}
