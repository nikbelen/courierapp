package com.example.courierapp.data

import com.example.courierapp.data.storage.UserStorage
import com.example.courierapp.domain.repository.LoginRepository
import javax.inject.Inject


class LoginRepositoryImplementation @Inject constructor(
    private val userStorage: UserStorage,
    private val loginAPIHelper: LoginAPIHelper
) : LoginRepository {
    override suspend fun getAccessToken(): String {
        return userStorage.getAccessToken()
    }

    override suspend fun setAccessToken(token: String): Boolean {
        userStorage.saveAccessToken(token)
        return true
    }

    override suspend fun getRefreshToken(): String {
        return userStorage.getRefreshToken()
    }

    override suspend fun setRefreshToken(token: String): Boolean {
        userStorage.saveRefreshToken(token)
        return true
    }

    override suspend fun getUserName(): String {
        return userStorage.getUserName()
    }

    override suspend fun setUserName(name: String): Boolean {
        userStorage.saveUserName(name)
        return true
    }

    override suspend fun tryLogin(user: String, pass: String): TokenDTO? {
        try {
            val resp = loginAPIHelper.login(LoginData(user, pass))
            return if (resp.isSuccessful) {
                resp.body()!!
            } else return null
        } catch (e: Exception) {
            return null
        }

    }

    override suspend fun refreshToken(user: String, token: String): TokenDTO? {
        try {
            val resp = loginAPIHelper.refreshToken(RefreshTokenData(user, token))
            return if (resp.isSuccessful) {
                resp.body()!!
            } else return null
        } catch (e: Exception) {
            return null
        }
    }

}