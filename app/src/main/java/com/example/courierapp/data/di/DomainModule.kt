package com.example.courierapp.data.di

import com.example.courierapp.domain.repository.LoginRepository
import com.example.courierapp.domain.usecases.LoginWithApiUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@Module
@InstallIn(ViewModelComponent::class)
class DomainModule {
    @Provides
    fun provideLoginWithApiUseCase(loginRepository: LoginRepository): LoginWithApiUseCase {
        return LoginWithApiUseCase(loginRepository)
    }
}