package com.example.courierapp.data

import android.os.Parcelable
import com.example.courierapp.domain.repository.LoginRepository
import kotlinx.coroutines.runBlocking
import kotlinx.parcelize.Parcelize
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Route
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import javax.inject.Inject


data class TokenDTO(
    val token: String,
    val refreshToken: String,
    val type: String,
    val expires: String
)

@Parcelize
data class LoginData(val userName: String, val password: String) : Parcelable

@Parcelize
data class RefreshTokenData(val userName: String, val token: String) : Parcelable

interface LoginService {

    @Headers("Content-Type: application/json")
    @POST("/api/v3/admin/auth/login")
    suspend fun login(
        @Body userData: LoginData
    ): Response<TokenDTO>

    @Headers("Content-Type: application/json")
    @POST("/api/v3/admin/auth/refresh")
    suspend fun refreshToken(
        @Body refreshTokenData: RefreshTokenData
    ): Response<TokenDTO>
}

class LoginAPIHelper @Inject constructor(loginRetrofit: Retrofit) {
    private val loginService: LoginService


    init {
        loginService = loginRetrofit.create(LoginService::class.java)
    }

    suspend fun login(loginData: LoginData): Response<TokenDTO> {
        return loginService.login(loginData)
    }

    suspend fun refreshToken(refreshTokenData: RefreshTokenData): Response<TokenDTO> {
        return loginService.refreshToken(refreshTokenData)
    }
}

class LoginAuthenticator @Inject constructor(private val loginRepository: LoginRepository) :
    Authenticator {
    override fun authenticate(route: Route?, response: okhttp3.Response): Request? {
        return runBlocking {
            when (val tokenDTO = loginRepository.refreshToken(
                loginRepository.getUserName(),
                loginRepository.getRefreshToken()
            )) {
                is TokenDTO -> {
                    loginRepository.setAccessToken(tokenDTO.token)
                    loginRepository.setRefreshToken(tokenDTO.refreshToken)
                    response.request.newBuilder()
                        .header("Authorization", "Bearer ${tokenDTO.token}")
                        .build()
                }

                else -> null
            }
        }
    }
}