package com.example.courierapp.data.storage

import android.content.Context

class SharedPrefsUserStorage(context: Context) : UserStorage {
    private val sharedPreferences =
        context.getSharedPreferences("shared_prefs", Context.MODE_PRIVATE)

    override fun getAccessToken(): String {
        return sharedPreferences.getString("access_token", "") ?: ""
    }

    override fun saveAccessToken(token: String): Boolean {
        sharedPreferences.edit().putString("access_token", token).apply()
        return true
    }

    override fun getRefreshToken(): String {
        return sharedPreferences.getString("refresh_token", "") ?: ""
    }

    override fun saveRefreshToken(token: String): Boolean {
        sharedPreferences.edit().putString("refresh_token", token).apply()
        return true
    }

    override fun getUserName(): String {
        return sharedPreferences.getString("username", "") ?: ""
    }

    override fun saveUserName(username: String): Boolean {
        sharedPreferences.edit().putString("username", username).apply()
        return true
    }
}