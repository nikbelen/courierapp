package com.example.courierapp.data.storage

interface UserStorage {
    fun getAccessToken(): String

    fun saveAccessToken(token: String): Boolean

    fun getRefreshToken(): String

    fun saveRefreshToken(token: String): Boolean

    fun getUserName(): String

    fun saveUserName(username: String): Boolean
}