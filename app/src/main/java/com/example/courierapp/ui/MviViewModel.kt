package com.example.courierapp.ui

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.StateFlow

abstract class MviViewModel<State : UiState, Event : UiEvent, Effect : UiEffect> : ViewModel() {

    val state: StateFlow<State>
        get() = reducer.state

    val effect: Flow<Effect>
        get() = reducer.effect

    protected fun sendEvent(event: Event) {
        reducer.sendEvent(event)
    }

    protected open val reducer: MviReducer<State, Event, Effect> = MviReducer()
}