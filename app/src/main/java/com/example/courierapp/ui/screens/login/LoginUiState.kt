package com.example.courierapp.ui.screens.login

import com.example.courierapp.ui.UiState

data class LoginUiState(
    val isLoading: Boolean,
    val login: String,
    val password: String
) : UiState