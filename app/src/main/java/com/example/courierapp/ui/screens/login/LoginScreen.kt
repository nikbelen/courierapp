package com.example.courierapp.ui.screens.login

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.navigation.NavController

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LoginScreen(
    navController: NavController,
    modifier: Modifier = Modifier,
) {
    val viewModel = hiltViewModel<LoginViewModel>()
    val uiState = viewModel.state.collectAsStateWithLifecycle()
    Scaffold()
    {
        Column(
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = Modifier
                .padding(it)
                .padding(32.dp)
                .fillMaxWidth()
                .fillMaxHeight()
        ) {
            Text(text = "Вход", fontSize = 34.sp, modifier = Modifier.padding(8.dp))

            OutlinedTextField(
                value = uiState.value.login,
                placeholder = { Text(text = "") },
                singleLine = true,
                label = { Text(text = "Логин") },
                onValueChange = { viewModel.onLoginChanged(LoginSetLoginUiEvent(it)) },
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )

            OutlinedTextField(
                value = uiState.value.password,
                placeholder = { Text(text = "") },
                singleLine = true,
                visualTransformation = PasswordVisualTransformation(),
                label = { Text(text = "Пароль") },
                onValueChange = { viewModel.onPasswordChanged(LoginSetPasswordUiEvent(it)) },
                enabled = true,
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            )

            TextButton(
                onClick = {
                    viewModel.onTryLogin(
                        LoginTryLoginUiEvent(
                            uiState.value.login,
                            uiState.value.password
                        )
                    )
                },
                colors = ButtonDefaults.buttonColors(),
                shape = RoundedCornerShape(4.dp),
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth()
            ) {
                Text(text = "ВОЙТИ")
            }
        }
    }
}