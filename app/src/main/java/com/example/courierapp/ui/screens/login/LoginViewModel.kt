package com.example.courierapp.ui.screens.login

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.example.courierapp.domain.usecases.LoginWithApiUseCase
import com.example.courierapp.ui.MviReducer
import com.example.courierapp.ui.MviViewModel
import com.example.courierapp.ui.UiEffect
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val loginWithApiUseCase: LoginWithApiUseCase
) : MviViewModel<LoginUiState, LoginUiEvent, UiEffect>() {
    override val reducer: MviReducer<LoginUiState, LoginUiEvent, UiEffect> = Reducer()

    init {
    }

    fun onLoginChanged(event: LoginSetLoginUiEvent) {
        reducer.sendEvent(event)
    }

    fun onPasswordChanged(event: LoginSetPasswordUiEvent) {
        reducer.sendEvent(event)
    }

    fun onTryLogin(event: LoginTryLoginUiEvent) {
        reducer.sendEvent(event)
    }

    private inner class Reducer :
        MviReducer<LoginUiState, LoginUiEvent, UiEffect>(LoginUiState(true, "", "")) {
        override fun reduce(oldState: LoginUiState, event: LoginUiEvent) {
            Log.d("AAA", "Reduce is called with event - $event")
            when (event) {
                is LoginSetLoginUiEvent -> {
                    setState(LoginUiState(true, event.login, oldState.password))
                }

                is LoginSetPasswordUiEvent -> {
                    setState(LoginUiState(true, oldState.login, event.password))
                }

                is LoginTryLoginUiEvent -> {
                    Log.d("AAA", "Trying to login with $oldState")
                    viewModelScope.launch {
                        val tokenDTO = loginWithApiUseCase.execute(event.login, event.password)
                        Log.d("AAA", "Sent POST, ans = $tokenDTO")
                    }
                }
            }
        }
    }
}

