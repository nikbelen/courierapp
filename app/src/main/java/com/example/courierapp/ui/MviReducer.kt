package com.example.courierapp.ui

import android.util.Log
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.receiveAsFlow

open class MviReducer<State : UiState, Event : UiEvent, Effect : UiEffect>(
    initialVal: State,
) {
    @Suppress("UNCHECKED_CAST")
    constructor() : this(UiState.None as State)

    private val _state: MutableStateFlow<State> = MutableStateFlow(initialVal)
    val state: StateFlow<State>
        get() = _state

    private val _effect: Channel<Effect> = Channel(Int.MAX_VALUE)
    val effect: Flow<Effect>
        get() = _effect.receiveAsFlow()


    fun sendEvent(event: Event) {
        Log.d("AAA", "Send event is called with $event")

        reduce(_state.value, event)
    }

    fun sendEffect(effect: Effect) {
        Log.d("AAA", "Send effect is called with $effect")

        _effect.trySend(effect)
    }

    fun setState(newState: State): State {
        val oldState = _state.value
        Log.d("AAA", "State has been changed. $oldState -> $newState")

        val success = _state.tryEmit(newState)
        return newState
    }

    protected open fun reduce(oldState: State, event: Event) {
        Log.d("AAA", "Reduce is called with event - $event")
    }
}

interface UiState {

    object None : UiState
}

interface UiEvent

interface UiEffect