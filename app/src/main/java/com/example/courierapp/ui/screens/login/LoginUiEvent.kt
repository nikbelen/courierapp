package com.example.courierapp.ui.screens.login

import com.example.courierapp.ui.UiEvent

interface LoginUiEvent : UiEvent

class LoginSetLoginUiEvent(val login: String) : LoginUiEvent

class LoginSetPasswordUiEvent(val password: String) : LoginUiEvent

class LoginTryLoginUiEvent(val login: String, val password: String) : LoginUiEvent