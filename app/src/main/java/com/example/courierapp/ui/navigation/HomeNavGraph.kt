package com.example.courierapp.ui.navigation

import androidx.navigation.NavController
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import androidx.navigation.navigation
import com.example.courierapp.ui.screens.Screen
import com.example.courierapp.ui.screens.login.LoginScreen

fun NavGraphBuilder.loginNavGraph(navController: NavController) {
    navigation(
        startDestination = Screen.Login.route,
        route = LOGIN_GRAPH_ROUTE
    ) {
        composable(Screen.Login.route) {
            LoginScreen(navController)
        }
    }
}