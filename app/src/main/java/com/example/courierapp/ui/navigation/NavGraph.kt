package com.example.courierapp.ui.navigation

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost

const val LOGIN_GRAPH_ROUTE = "home"

@Composable
fun Navigation(navController: NavHostController, modifier: Modifier) {
    NavHost(
        navController = navController,
        startDestination = LOGIN_GRAPH_ROUTE,
        modifier = modifier
    ) {
        loginNavGraph(navController = navController)
    }
}