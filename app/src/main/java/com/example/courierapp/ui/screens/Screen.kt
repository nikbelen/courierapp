package com.example.courierapp.ui.screens

sealed class Screen(val route: String) {
    object Login : Screen("login_screen")
}