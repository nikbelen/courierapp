package com.example.courierapp.app;

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class CourierApp : Application() {

}