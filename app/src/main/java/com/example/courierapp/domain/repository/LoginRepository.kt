package com.example.courierapp.domain.repository

import com.example.courierapp.data.TokenDTO

interface LoginRepository {
    suspend fun getAccessToken(): String

    suspend fun setAccessToken(token: String): Boolean

    suspend fun getRefreshToken(): String

    suspend fun setRefreshToken(token: String): Boolean

    suspend fun getUserName(): String

    suspend fun setUserName(name: String): Boolean

    suspend fun tryLogin(user: String, pass: String): TokenDTO?

    suspend fun refreshToken(user: String, token: String): TokenDTO?
}