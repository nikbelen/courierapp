package com.example.courierapp.domain.usecases

import android.util.Log
import com.example.courierapp.domain.repository.LoginRepository
import javax.inject.Inject

class LoginWithApiUseCase @Inject constructor(private val loginRepository: LoginRepository) {
    suspend fun execute(user: String, pass: String): Boolean {
        val tokenDTO = loginRepository.tryLogin(user, pass)
        Log.d("AAA", "Sent POST, ans = $tokenDTO")
        if (tokenDTO != null) {
            loginRepository.setAccessToken(tokenDTO.token)
            return true
        }
        return false
    }
}